<p style="text-align:center">
<img height="148" width="330" src="../assets/title.svg">
<br>
<i>A game of quick thinking and shape shifting!</i>
</p>

## Contents

- 4 diamond game pieces
- 60 playing cards (triangular polysticks)
- 41 playing cards (diamond polyominoes)

## Objective

Win by being the first player to successfully form the patterns of all your cards.

### Game Info

- Ages: 8+
- Players: 1-5
- Play Time: 10-15 minutes

#### Warning

Choking Hazard - Small Parts  
Not for children under 3 years

## Set Up

Shuffle the deck of cards to make the draw pile. Flip up the top card of the draw pile to form the discard pile. Form the pattern of this card out of the diamond pieces.

## How to Match

The pattern on the cards may be matched right-side up, upside down, mirror image (reflected), or any rotation of those positions.  
<img height="120" width="354" src="../assets/figure1.svg">  
_Figure #1: Example of a pattern and its upside down and mirror image arrangements._

To form the exact angles for a pattern, the diamond shape provides a useful guide. In Figure #2, position **A** shows two lines with an open, obtuse angle. To form the obtuse angle, make sure the sides of the two diamonds are in a straight line. Position **B** shows two lines in a straight line. Just make the lines on the diamonds form a straight line to match the pattern. Position **C** is a sharp, acute angle. Bringing the sides of the two diamond pieces together makes the acute angle.  
<img height="350" width="475" src="../assets/figure2.jpg">  
_Figure #2: Example of diamond game piece placement._

## Game Play

### Standard Game Mode

Deal _**seven**_ cards from the draw pile to each player (five cards when playing with diamond deck). Players may look at their cards and keep them concealed from the other players.

The player to the left of the dealer goes first, and play moves in a clockwise direction.

On your turn, you may play a card from your hand and place it on top of the discard pile. If you cannot play a card from your hand, you must draw up to two cards, one at a time, from the draw pile. Your turn ends when you either play a card or draw two cards. If you have a card that _can_ be played, it _must_ be played.

To play a card from your hand, of the four diamond game pieces take _One Off_. Using this removed diamond, match the pattern of the card without moving the remaining three diamonds.

Over the course of the game, if there are four turns in a row when a player took a card from the draw pile (whether they were able to play the cards or not), the discard pile gets reshuffled into the draw pile. If the draw pile goes empty, the discard pile gets reshuffled into the draw pile. When reshuffling, always leave the card matching the pattern formed by the game pieces as the only remaining card of the discard pile.

The first player to run out of cards wins!

_Optional Rule_: It is possible for the pattern formed by the remaining three diamond game pieces to become disconnected when taking _One Off_. When this happens, you may choose a player to draw a card!  
<img height="120" width="354" src="../assets/figure3.svg">  
_Figure #3: Example of a transition between two patterns that passes through a disconnected state._

### Advanced Game Mode

_To be added later_

### Solitaire Game Mode

_To be added later_

## More About the Math

### Triangular Polysticks

[The On-Line Encyclopedia of Integer Sequences - Number of 2-sided n-triangular polyedges](http://oeis.org/A159867)  
[Wolfram Mathworld - Polystick](https://mathworld.wolfram.com/Polystick.html)  
[Wikipedia - Polystick](https://en.wikipedia.org/wiki/Polystick)

### Diamond Polyominoes

[The On-Line Encyclopedia of Integer Sequences - Number of polydiamonds: polyominoes made from n diamonds](https://oeis.org/A056845)

## Credits

_To be added later_
